import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { StreetComponent } from './components/street/street.component';
import { CustomComponent } from './components/custom/custom.component';
import { TrailComponent } from './components/trail/trail.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroUsuarioComponent } from './components/cadastro-usuario/cadastro-usuario.component';
import { CadastroClienteComponent } from './components/cadastro-cliente/cadastro-cliente.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { AcompanhamentoComponent } from './components/acompanhamento/acompanhamento.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StreetComponent,
    CustomComponent,
    TrailComponent,
    ScooterComponent,
    SportComponent,
    TouringComponent,
    LoginComponent,
    CadastroUsuarioComponent,
    CadastroClienteComponent,
    CadastroMotosComponent,
    AcompanhamentoComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
